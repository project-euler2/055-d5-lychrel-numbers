import time
start_time = time.time()

def count_lychrel_numbers():
    count_lychrel = 0
    for i in range(10,10000):
        j = 0
        last_num = i + int(str(i)[::-1])
        while True:
            if j > 50:
                count_lychrel +=1
                break
            if str(last_num) == str(last_num)[::-1]:
                break
            last_num = last_num + int(str(last_num)[::-1])
            j +=1
    return count_lychrel

print(count_lychrel_numbers())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )